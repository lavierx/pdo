<?php
/**
 * @copyright 2005-2008 OpenPNE Project
 * @license   http://www.php.net/license/3_01.txt PHP License 3.01
 */

/**
 * コメント削除
 */
class pc_do_c_bbs_delete_c_commu_topic_comment extends OpenPNE_Action
{
    function execute($requests)
    {
        $u = $GLOBALS['AUTH']->uid();

        $target_c_commu_topic_comment_id = $requests['target_c_commu_topic_comment_id'];
        
	//-------------------いいね削除
	//$CMB="<!--IB;31-409-3;1--><span style='color:green'>>>409　確認しました!</span>";
	$CMB=$_POST["CMB"];
	$CMB2=explode(";", $CMB);
	if($CMB2[0]=="<!--IB"){
		 //配列をファイルから読み込み
		$IINE = unserialize(file_get_contents("//public_html/sns2/public_html/iine.dat"));
		//ライブラリパス追加 //////////////////////////////////////
		$path="//public_html/sns/lib/include";//OpenPNE/lib/includeへのパス
		set_include_path(get_include_path() . PATH_SEPARATOR . $path); //ライブラリパスを追加
		//データベースに接続 //////////////////////////////////////
		$con = mysql_connect("DBHOST", "USER","PASS");
		///////////////////////////////////////////////////////////
		//UTF8の文字化け対策
		mysql_query('SET NAMES utf8', $con); // ←これ
		//データベースを選択////////////////////////////////////////
		mysql_select_db("DBNAME", $con);
		//SQL文をセット/////////////////////////////////////////////
		$quryset = mysql_query("SELECT  `c_member_id` ,  `nickname` ,  `image_filename` 
		FROM  `c_member` 
		WHERE  `c_member_id` =".$CMB2[2]."
		LIMIT 0 , 1");
		////////////////////////////////////////////////////////////
		//１ループで１行データが取り出され、データが無くなるとループを抜けます。
		while ($data = mysql_fetch_array($quryset)){
		        $nname = $data[1];
		        $nname2 = "<img height=40 title='".$nname."' src=//public_html/img.php?filename=".$data[2]."&w=76&h=76&m=pc>";
		}
		
		//配列の中身を書き換える!
		$IINE[$CMB2[1]] = str_replace("&nbsp;".$nname2, "", $IINE[$CMB2[1]]);
		//配列の中身をファイルに保存
		file_put_contents("//public_html/sns2/public_html/iine.dat", serialize($IINE));
	}
	//-------------------いいね削除

        //--- 権限チェック
        //コミュニティ管理者 or コミュニティ参加者

        $c_commu_topic_comment = db_commu_c_commu_topic_comment4c_commu_topic_comment_id_2($target_c_commu_topic_comment_id);

        $c_commu_topic = db_commu_c_commu_topic4c_commu_topic_id($c_commu_topic_comment['c_commu_topic_id']);
        $c_commu_id = $c_commu_topic['c_commu_id'];

        $status = db_common_commu_status($u, $c_commu_id);
        if ($c_commu_topic_comment['number']=="0") {
            handle_kengen_error();
        }
        if (!$status['is_commu_admin']
            && $c_commu_topic_comment['c_member_id'] != $u) {
            handle_kengen_error();
        }
        //---

        db_commu_delete_c_commu_topic_comment($target_c_commu_topic_comment_id);

        if ($c_commu_topic['event_flag']) {
            $action = 'page_c_event_detail';
        } else {
            $action = 'page_c_topic_detail';
        }
        $p = array('target_c_commu_topic_id' => $c_commu_topic_comment['c_commu_topic_id']);
        openpne_redirect('pc', $action, $p);
    }
}

?>
